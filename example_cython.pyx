from sage.misc.cachefunc import cached_method

cdef class A:
    def one(self):
        r"""
        Return 1
        """
        return 1

    @cached_method
    def two(self):
        r"""
        Return 2
        """
        return 2
