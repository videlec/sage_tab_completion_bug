from sage.misc.cachefunc import cached_method

class A:
    @cached_method
    def one(self):
        r"""
        Return 1
        """
        return 1
