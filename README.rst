Bug in __doc__ (sageinspect)
============================

This package contains a minimal example to reproduce a bug related to sage inspection
module when trying to recover the documentation of a cached_method of an extension
class. This was tested with a compiled sage 9.3.rc2 on archlinux.

Procedure to reproduce

**Zero step:** clone the repo and `cd` to the cloned repo

**First step:** Cython compilation ::

    $ sage -python setup.py build_ext --inplace

**Second step:** launch `sage` and run::

    sage: import example_cython
    sage: a = example_cython.A()
    sage: import example_cython
    sage: a = example_cython.A()
    sage: a.one.__doc__  # works fine
    '\n        Return 1\n        '
    sage: a.two.__doc__  # does not work
    ---------------------------------------------------------------------------
    TypeError                                 Traceback (most recent call last)
    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2377             # and str (=unicode) in python3
    -> 2378             return inspect.getsourcelines(obj)
       2379

    /usr/lib/python3.9/inspect.py in getsourcelines(object)
       1005     object = unwrap(object)
    -> 1006     lines, lnum = findsource(object)
       1007

    /usr/lib/python3.9/inspect.py in findsource(object)
        816
    --> 817     file = getsourcefile(object)
        818     if file:

    /usr/lib/python3.9/inspect.py in getsourcefile(object)
        696     """
    --> 697     filename = getfile(object)
        698     all_bytecode_suffixes = importlib.machinery.DEBUG_BYTECODE_SUFFIXES[:]

    /usr/lib/python3.9/inspect.py in getfile(object)
        676         return object.co_filename
    --> 677     raise TypeError('module, class, method, function, traceback, frame, or '
        678                     'code object was expected, got {}'.format(

    TypeError: module, class, method, function, traceback, frame, or code object was expected, got method_descriptor

    During handling of the above exception, another exception occurred:

    TypeError                                 Traceback (most recent call last)
    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2377             # and str (=unicode) in python3
    -> 2378             return inspect.getsourcelines(obj)
       2379

    /usr/lib/python3.9/inspect.py in getsourcelines(object)
       1005     object = unwrap(object)
    -> 1006     lines, lnum = findsource(object)
       1007

    /usr/lib/python3.9/inspect.py in findsource(object)
        816
    --> 817     file = getsourcefile(object)
        818     if file:

    /usr/lib/python3.9/inspect.py in getsourcefile(object)
        696     """
    --> 697     filename = getfile(object)
        698     all_bytecode_suffixes = importlib.machinery.DEBUG_BYTECODE_SUFFIXES[:]

    /usr/lib/python3.9/inspect.py in getfile(object)
        665                 return module.__file__
    --> 666         raise TypeError('{!r} is a built-in class'.format(object))
        667     if ismethod(object):

    TypeError: <class 'method_descriptor'> is a built-in class

    During handling of the above exception, another exception occurred:

    TypeError                                 Traceback (most recent call last)
    <ipython-input-4-812a7190259e> in <module>
    ----> 1 a.two.__doc__

    /opt/sage/local/lib/python3.9/site-packages/sage/docs/instancedoc.pyx in sage.docs.instancedoc.InstanceDocDescriptor.__get__ (build/cythonized/sage/docs/instancedoc.c:1800)()
        209                 return doc
        210
    --> 211         return self.instancedoc(obj)
        212
        213     def __set__(self, obj, value):

    /opt/sage/local/lib/python3.9/site-packages/sage/misc/cachefunc.pyx in sage.misc.cachefunc.CachedFunction._instancedoc_ (build/cythonized/sage/misc/cachefunc.c:5053)()
        875         if not doc or _extract_embedded_position(doc) is None:
        876             try:
    --> 877                 sourcelines = sage_getsourcelines(f)
        878                 from sage.env import SAGE_SRC, SAGE_LIB
        879                 filename = sage_getfile(f)

    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2395                             return sage_getsourcelines(B)
       2396                     if obj.__class__ != type:
    -> 2397                         return sage_getsourcelines(obj.__class__)
       2398                     raise err
       2399

    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2393                             B = None
       2394                         if B is not None and B is not obj:
    -> 2395                             return sage_getsourcelines(B)
       2396                     if obj.__class__ != type:
       2397                         return sage_getsourcelines(obj.__class__)

    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2396                     if obj.__class__ != type:
       2397                         return sage_getsourcelines(obj.__class__)
    -> 2398                     raise err
       2399
       2400     (orig, filename, lineno) = pos

    /opt/sage/local/lib/python3.9/site-packages/sage/misc/sageinspect.py in sage_getsourcelines(obj)
       2376             # inspect gives str (=bytes) in python2
       2377             # and str (=unicode) in python3
    -> 2378             return inspect.getsourcelines(obj)
       2379
       2380         except (IOError, TypeError) as err:

    /usr/lib/python3.9/inspect.py in getsourcelines(object)
       1004     raised if the source code cannot be retrieved."""
       1005     object = unwrap(object)
    -> 1006     lines, lnum = findsource(object)
       1007
       1008     if istraceback(object):

    /usr/lib/python3.9/inspect.py in findsource(object)
        815     is raised if the source code cannot be retrieved."""
        816
    --> 817     file = getsourcefile(object)
        818     if file:
        819         # Invalidate cache if needed.

    /usr/lib/python3.9/inspect.py in getsourcefile(object)
        695     Return None if no way can be identified to get the source.
        696     """
    --> 697     filename = getfile(object)
        698     all_bytecode_suffixes = importlib.machinery.DEBUG_BYTECODE_SUFFIXES[:]
        699     all_bytecode_suffixes += importlib.machinery.OPTIMIZED_BYTECODE_SUFFIXES[:]

    /usr/lib/python3.9/inspect.py in getfile(object)
        664             if getattr(module, '__file__', None):
        665                 return module.__file__
    --> 666         raise TypeError('{!r} is a built-in class'.format(object))
        667     if ismethod(object):
        668         object = object.__func__

    TypeError: <class 'object'> is a built-in class

Tentative explanation
---------------------

The two methods have different types::

    sage: type(a.one)
    <class 'builtin_function_or_method'>
    sage: type(a.two)  # the wrapper introduced by cached_method
    <class 'sage.misc.cachefunc.CachedMethodCallerNoArgs'>
    sage: type(a.two.f)  # the underlying method
    <class 'method_descriptor'>

And indeed a `method_descriptor` confuses the inspect module::

    sage: import inspect
    sage: inspect.getfile(a.two.f)
    ---------------------------------------------------------------------------
    TypeError                                 Traceback (most recent call last)
    <ipython-input-7-a88b731925a6> in <module>
    ----> 1 inspect.getfile(a.two.f)

    /usr/lib/python3.9/inspect.py in getfile(object)
        675     if iscode(object):
        676         return object.co_filename
    --> 677     raise TypeError('module, class, method, function, traceback, frame, or '
        678                     'code object was expected, got {}'.format(
        679                     type(object).__name__))

    TypeError: module, class, method, function, traceback, frame, or code object was expected, got method_descriptor

The `method_descriptor` seems to have been introduced in recent Python versions. It appears
that it confuses a lot the sageinspect module.

Remark on tab-completion
------------------------

This bug was originally found by playing with tab-completion::

    sage: import example_cython
    sage: a = example_cython.A()
    sage: a.o<TAB>  # correctly expand to one
    sage: a.t<TAB>  # shows the same trace as above

It is a bit unclear why the tab-completion would be related in any way to the `__doc__` attribute.
